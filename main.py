import asyncio
from redis import asyncio as redis
from fastapi import FastAPI

app = FastAPI()

r = redis.from_url("redis://localhost")
@app.get("/")
async def root() -> dict[str, str]:
    # value = await r.get("my-key")
    # if not value:
    #     await r.set("my-key", "value")
    # await asyncio.sleep(0.1)
    return {"hello": "world!"}

# uvicorn main:app --host 192.168.1.19 --port 8000 --workers 4 --loop 'uvloop'
# python3 -m socketify main:app --host 192.168.1.18 --port 8000 --workers 4 