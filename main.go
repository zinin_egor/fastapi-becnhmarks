package main

import (
	"log"

	"github.com/goccy/go-json"
	"github.com/gofiber/fiber/v2"
)

func main() {
	app := fiber.New(fiber.Config{
		JSONEncoder: json.Marshal,
		JSONDecoder: json.Unmarshal,
		Prefork:     true,
	})
	app.Get("/", hello)
	log.Fatal(app.Listen("192.168.1.19:8000"))
}

func hello(c *fiber.Ctx) error {
	return c.SendString("Hello, World!")
}
