import http from 'k6/http';
// import { sleep } from 'k6';
export const options = {
  //scenario to view contacts
  scenarios: {
    constant_request_rate: {
      executor: 'constant-arrival-rate',
      rate: 1000,
      timeUnit: '0.01s', // 100000  max 72k iterations per second, i.e. 1000 RPS
      duration: '60s',
      preAllocatedVUs: 100, // how large the initial pool of VUs would be
      maxVUs: 300, // if the preAllocatedVUs are not enough, we can initialize more
    },
  },
};
export default function () {
  http.get('http://:8000/');
}